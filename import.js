const rp = require('request-promise');

const JIRA_URL = 'https://YOUR_URL.atlassian.net/'; // JIRA base URL
const JIRA_PROJECT = 'PROJ'; // JIRA short project ID
let username = 'your-username@example.com'; // JIRA username
let password = 'password'; // JIRA password
const JIRA_ACCOUNT = { // JIRA account object
  username,
  password
};

const GITLAB_URL = 'https://gitlab.com/'; // GitLab base URL
const GITLAB_PROJECT = 'namespace/project'; // GitLab project
const GITLAB_ACCOUNT = { // GitLab account object
  username,
  password
};

const GITLAB_TOKEN = 'API_TOKEN'; // API token
const gitlabUsername = 'your-username'; // GitLab username to which to assign issues
const projectId = 12345; // GitLab project ID
const offset = 0; // Pagination start
const limit = 1000; // Pagination items per page

async function importJira() {
  console.log(`Logging into Bitbucket...`);

  let result = await rp({
    method: 'get',
    uri: `${JIRA_URL}/rest/api/2/search?jql=project=${JIRA_PROJECT}+order+by+id+asc&startAt=${offset}&maxResults=${limit}`,
    json: true,
    headers: {
      'Authorization': "Basic " + new Buffer(JIRA_ACCOUNT.username + ":" + JIRA_ACCOUNT.password).toString("base64")
    }
  });

  console.log(`Got project details for ${JIRA_PROJECT} from Bitbucket (${result.issues.length} issues)`);

  /* this token will be used whenever the API is invoked and
  * the jira's author of (the comment / attachment / issue) is not a gitlab user.
  * So, this identity will be used instead.
  * GITLAB_TOKEN is visible in your account: https://ci.linagora.com/profile/account
  */

  console.log('Logging into JIRA to retrieve issues...');

  let issues = result.issues;
  for (const JIRA_ISSUE of issues) {
    let issue = await rp({
      method: 'get',
      uri: `${JIRA_URL}/rest/api/2/issue/${JIRA_ISSUE.id}/?fields=attachment,comment,description,summary,issuetype,created,updated,status,attachments`,
      json: true,
      headers: {
        'Authorization': "Basic " + new Buffer(JIRA_ACCOUNT.username + ":" + JIRA_ACCOUNT.password).toString("base64")
      }
    });

    let JIRAIssue = issue;
    let JIRAComments = JIRAIssue.fields.comment.comments || [];
    let JIRAAttachments = JIRAIssue.fields.attachments || [];

    console.log(`${JIRAIssue.key} - ${JIRAIssue.fields.summary}`);

    let gitlabIssue = {
      title:       JIRAIssue.fields.summary,
      description: JIRAIssue.fields.description,
      labels:      JIRAIssue.fields.issuetype.name,
      created_at:  JIRAIssue.fields.created,
      updated_at:  JIRAIssue.fields.updated,
      done:        JIRAIssue.fields.status.statusCategory.name === 'Done',
      assignee:    gitlabUsername,
      reporter:    gitlabUsername,
      comments:    JIRAComments.map(JIRAComment => ({
        author:     gitlabUsername,
        comment:    JIRAComment.body,
        created_at: JIRAComment.created
      })),
      attachments: JIRAAttachments.map(JIRAAttachment => ({
        author:     gitlabUsername,
        filename:   JIRAAttachment.filename,
        content:    JIRAAttachment.content,
        created_at: JIRAAttachment.created
      }))
    };

    // Create the GitLab issue
    await rp({
      method: 'post',
      uri: `${GITLAB_URL}/api/v4/projects/${projectId}/issues`,
      qs: gitlabIssue,
      json: true,
      headers: {
        'PRIVATE-TOKEN': GITLAB_TOKEN
      }
    });
  }
}

(async () => {
  try {
    await importJira();
  } catch (e) {
    console.trace(e);
  }
})();
