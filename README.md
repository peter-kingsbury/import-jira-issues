# Import Atlassian JIRA Issues into GitLab

1. Run `npm install`.
2. Create your GitLab API key.
3. Obtain your JIRA project ID.
4. Populate the variables at the top of the script.
5. Run `node import.js`.
6. Verify success in GitLab Issues page.

For any questions please contact <peter.kingsbury@gmail.com>

Inspired by [these gists](https://about.gitlab.com/2017/08/21/migrating-your-jira-issues-into-gitlab/) and adapted for request-promise.
